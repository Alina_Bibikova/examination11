import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Switch, withRouter} from "react-router-dom";

import {NotificationContainer} from "react-notifications";
import {logoutUser} from "../src/store/actions/usersActions";

import Toolbar from "../src/components/UI/Toolbar/Toolbar";
import {connect} from "react-redux";
import {Route} from "react-router";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Products from "./containers/Products/Products";
import NewProduct from "./containers/NewProduct/NewProduct";
import InfoProduct from "./containers/InfoProduct/InfoProduct";

class App extends Component {
  render() {
    return (
      <Fragment>
          <NotificationContainer/>
        <header>
          <Toolbar user={this.props.user}
                   logout={this.props.logoutUser}/>
        </header>
        <Container style={{marginTop: '20px'}}>
          <Switch>
            <Route path="/" exact component={Products} />
            <Route path="/product/new" exact component={NewProduct} />
            <Route path="/register" exact component={Register} />
            <Route path="/login" exact component={Login} />
            <Route path="/products/:id" exact component={InfoProduct}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
