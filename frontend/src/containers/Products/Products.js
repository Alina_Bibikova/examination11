import React, {Component, Fragment} from 'react';
import ProductListItem from "../../components/ProductListItem/ProductListItem";
import {fetchProduct, fetchProducts} from "../../store/actions/productsActions";
import {connect} from "react-redux";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {NavLink as RouterNavLink} from 'react-router-dom';
import {Col, Nav, NavItem, NavLink, Row} from "reactstrap";

class Products extends Component {
    async componentDidMount() {
       await this.props.onFetchProducts();
       await  this.props.fetchCategories();
       this.props.fetchProduct(this.props.match.params.id);
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.id !== prevProps.match.params.id){
            this.props.fetchProduct(this.props.match.params.id);
        }
    }

    render() {
        return (
            <Fragment>
                <Row style={{marginTop: "20px"}}>
                    <Col sm={3}>
                        <Nav vertical>
                            <NavItem>
                                <NavLink tag={RouterNavLink} to="/" exact>All products</NavLink>
                            </NavItem>
                            {this.props.categories.map(categoryId => (
                                <NavItem key={categoryId._id}>
                                    <NavLink
                                        tag={RouterNavLink}
                                        to={"/products/" + categoryId}
                                        exact
                                    >
                                        {categoryId.title}</NavLink>
                                </NavItem>
                            ))}
                        </Nav>
                    </Col>

                {this.props.products ? <Col sm={9}>
                        {this.props.products.map(product => (
                            <ProductListItem
                                key={product._id}
                                _id={product._id}
                                title={product.title}
                                price={product.price}
                                image={product.image}
                            />
                        ))}
                    </Col> : null}
                </Row>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    products: state.product.products,
    categories: state.category.categories,
    product: state.product.product,
});

const mapDispatchToProps = dispatch => ({
    onFetchProducts: () => dispatch(fetchProducts()),
    fetchCategories: () => dispatch(fetchCategories()),
    fetchProduct: id => dispatch(fetchProduct(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);