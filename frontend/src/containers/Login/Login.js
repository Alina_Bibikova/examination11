import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {LoginUser} from "../../store/actions/usersActions";

class Login extends Component {
    state = {
        login: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value});
    };

    submitFromHandler = event => {
        event.preventDefault();

        this.props.loginUser({...this.state})
    };

    render() {
        return (
            <Fragment>
                <h2>Login</h2>
                {this.props.error && (
                    <Alert color="danger">
                        {this.props.error.message || this.props.error.global}
                    </Alert>
                )}
                <Form onSubmit={this.submitFromHandler}>

                    <FormElement
                        propertyName="login"
                        title="Login"
                        placeholder="Enter login your registered with"
                        value={this.state.login}
                        onChange={this.inputChangeHandler}
                        type="text"
                        autoComplete="current-login"
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        placeholder="Enter password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        type="password"
                        autoComplete="current-password"
                    />

                    <FormGroup row>
                        <Col sm={{offset:2, size: 10}}>
                            <Button
                                type="submit"
                                color="primary"
                            >
                                Login
                            </Button>

                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
   error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(LoginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);