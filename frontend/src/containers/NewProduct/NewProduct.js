import React, {Component, Fragment} from 'react';
import ProductForm from "../../components/ProductForm/ProductForm";
import {connect} from "react-redux";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {addProduct} from "../../store/actions/productsActions";

class NewProduct extends Component {
    componentDidMount() {
        this.props.fetchCategories();
    }

    createProduct = productData => {
        this.props.onProductCreated(productData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>New product</h2>
                <ProductForm
                    onSubmit={this.createProduct}
                    categories={this.props.categories}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.category.categories
});

const mapDispatchToProps = dispatch => ({
    onProductCreated: productData => dispatch(addProduct(productData)),
    fetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);
