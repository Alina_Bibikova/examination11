import React, {Component, Fragment} from 'react';
import InfoProductListItem from "../../components/InfoProductListItem/InfoProductListItem";
import {connect} from "react-redux";
import {deleteProduct, fetchProduct} from "../../store/actions/productsActions";

class InfoProduct extends Component {
    async componentDidMount() {
        await this.props.onfetchProduct(this.props.match.params.id);
    }

    render() {
        const product = this.props.product;
        const category = product.category && product.category.title;
        const user = this.props.user && this.props.user.displayName;

        return (
            this.props.product ? <Fragment>
                <h2>Info product</h2>
                <InfoProductListItem
                    image={this.props.product.image}
                    description={this.props.product.description}
                    title={this.props.product.title}
                    price={this.props.product.price}
                    deleteProduct={() => this.props.deleteProduct(this.props.match.params.id)}
                    category={category}
                    user={user}
                    phone={user}
                />
            </Fragment> : null
        );
    }
}

const mapStateToProps = state => ({
    product: state.product.product,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    onfetchProduct: id => dispatch(fetchProduct(id)),
    deleteProduct: id => dispatch(deleteProduct(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InfoProduct);