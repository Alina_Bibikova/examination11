import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import {connect} from "react-redux";
import {registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/Form/FormElement";

class Register extends Component {
    state = {
        login: '',
        password: '',
        displayName: '',
        phone: ''

    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value});
    };

    submitFromHandler = event => {
        event.preventDefault();

        this.props.registerUser({...this.state});
    };

    fieldHasError = fieldName => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message
    };

    render() {
        return (
            <Fragment>
                <h2>Register new user</h2>
                {this.props.error && this.props.error.global && (
                    <Alert color="danger">
                        {this.props.error.global}
                    </Alert>
                )}
                <Form onSubmit={this.submitFromHandler}>

                    <FormElement
                    propertyName="login"
                    title="login"
                    placeholder="Enter your desired login"
                    value={this.state.login}
                    onChange={this.inputChangeHandler}
                    error={this.fieldHasError('login')}
                    type="text"
                    autoComplete="new-login"
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        placeholder="Enter new secure password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        error={this.fieldHasError('password')}
                        type="password"
                        autoComplete="new-password"
                    />

                    <FormElement
                        propertyName="displayName"
                        title="Display name"
                        placeholder="Enter display name"
                        value={this.state.displayName}
                        onChange={this.inputChangeHandler}
                        error={this.fieldHasError('displayName')}
                        type="displayName"
                        autoComplete="new-displayName"
                    />

                    <FormElement
                        propertyName="phone"
                        title="Phone"
                        placeholder="Enter phone"
                        value={this.state.phone}
                        onChange={this.inputChangeHandler}
                        error={this.fieldHasError('phone')}
                        type="number"
                        autoComplete="new-phone"
                    />

                    <FormGroup row>
                        <Col sm={{offset:2, size: 10}}>
                            <Button
                                type="submit"
                                color="primary"
                            >
                                Register
                            </Button>

                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    registerUser: UserData => dispatch(registerUser(UserData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);