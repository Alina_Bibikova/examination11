import React from 'react';
import PropTypes from "prop-types";
import ForumThumbnail from "../ProductThumbnail/ProductThumbnail";
import {Button, Card, CardBody, CardFooter, CardText} from "reactstrap";

const InfoProductListItem = props => {
    return (
        <Card style={{marginTop: '10px', marginBottom: '10px'}}>
            <CardBody>
                <strong>{props.category}</strong>
                <ForumThumbnail image={props.image}/>
                <span>User - {props.user}: </span>
                <span>{props.phone}</span>
                <CardText>
                    Title: {props.title}
                </CardText>
                <CardText>
                    Description: {props.description}
                </CardText>
                <CardText>
                    Price: {props.price} $
                </CardText>
                {props.user ? <CardFooter>
                    <Button onClick={props.deleteProduct} style={{margin: '10px'}} color="danger">Delete</Button>
                </CardFooter> : null}
            </CardBody>
        </Card>
    );
};

InfoProductListItem.propTypes = {
    image: PropTypes.string,
    title: PropTypes.string,
};

export default InfoProductListItem;