import {FETCH_CATEGORIES_SUCCESS, FETCH_CATEGORY_SUCCESS, FETCH_DATA_FAILURE} from "../actions/categoriesActions";

const initialState = {
    categories: [],
    category: [],
    error: null,
    loading: null
};


const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, categories: action.categories, error: null};

        case FETCH_DATA_FAILURE:
            return {...state, error: action.error};

        case FETCH_CATEGORY_SUCCESS:
            return {...state, category: action.category};
        default:
            return state;
    }
};

export default categoriesReducer;