import {FETCH_DATA_FAILURE, FETCH_PRODUCT_SUCCESS, FETCH_PRODUCTS_SUCCESS} from "../actions/productsActions";

const initialState = {
    products: [],
    product: [],
    error: null,
    loading: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products, error: null};

        case FETCH_PRODUCT_SUCCESS:
            return {state, product: action.product, error: null};

        case FETCH_DATA_FAILURE:
            return {...state, error: action.error};

        default:
            return state;
    }
};

export default reducer;