import axios from '../../axios-api';

export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';
export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';

export const fetchCategoriesSuccess = categories => ({type: FETCH_CATEGORIES_SUCCESS, categories});
const fetchDataRequest = () => ({type: FETCH_DATA_REQUEST});
const fetchDataFailure = error => ({type: FETCH_DATA_FAILURE, error});
const fetchCategorySuccess = category => ({type: FETCH_CATEGORY_SUCCESS, category});


export const fetchCategories = () => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get('/categories');
            dispatch(fetchCategoriesSuccess(response.data));
        } catch (error) {
            dispatch(fetchDataFailure(error));

        }
    };
};

export const fetchCategory = id => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get(`/categories/${id}`);
            dispatch(fetchCategorySuccess(response.data));
        } catch (error) {
            dispatch(fetchDataFailure(error));

        }
    };
};

