import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';

export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';

export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const ADD_DATA_REQUEST = 'ADD_DATA_REQUEST';
export const ADD_DATA_FAILURE = 'ADD_DATA_FAILURE';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';

export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, products});
export const createProductSuccess = () => ({type: CREATE_PRODUCT_SUCCESS});
const fetchDataRequest = () => ({type: FETCH_DATA_REQUEST});
const fetchDataFailure = error => ({type: FETCH_DATA_FAILURE, error});
const addDataRequest = () => ({type: ADD_DATA_REQUEST});
const addDataFailure = error => ({type: ADD_DATA_FAILURE, error});
const fetchProductSuccess = product => ({type: FETCH_PRODUCT_SUCCESS, product});

export const fetchProducts = () => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get('/products');
            dispatch(fetchProductsSuccess(response.data));
        } catch (error) {
            dispatch(fetchDataFailure(error));
        }
    }
};

export const fetchProduct = id => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get(`/products/${id}`);
            dispatch(fetchProductSuccess(response.data));
        } catch (error) {
            dispatch(fetchDataFailure(error));
        }
    }
};

export const addProduct = productData => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        dispatch(addDataRequest());

        try {
            await axios.post('/products', productData, config);

            dispatch(createProductSuccess());
            NotificationManager.success("New product!");
        } catch (error) {
            dispatch(addDataFailure(error.response.data));
        }
    }
};

export const deleteProduct = id => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        dispatch(fetchDataRequest());

        try {
            await axios.delete(`/products/${id}`, config);
            NotificationManager.success('Delete product!');
        } catch (error) {
            dispatch(fetchDataFailure(error));
        }
    };
};
